﻿using System;
using System.Threading.Tasks;
using SampleCore2DataAccess.Model;

namespace SampleCore2DataAccess
{
    public class SampleData
    {
        //private readonly IRepository _repository;

        //public SampleData(IRepository repository)
        //{
        //    _repository = repository;
        //}

        public string[] GetAll()
        {
            return new string[] { "value111", "value222" };
        }
    }
}
